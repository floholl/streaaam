;;; Source:
;;; https://learn.bela.io/using-bela/languages/csound/#a-simple-instrument

<CsoundSynthesizer>

<CsOptions>
-odac
</CsOptions>

<CsInstruments>

sr = 44100
ksmps = 8
nchnls = 2
0dbfs = 1.0

instr 1
  aNoise pinker                 ; pink noise
  aSin poscil 0.4, p4           ; sine oscillator
  aLFO1 lfo 0.7, p5, 3          ; LFO - square (unipolar)
  aLFO2 lfo 0.7, .3, 2          ; LFO - square (bipolar)
  aSig = (aLFO1 * aNoise) + (aLFO2 * aSin)  ; crazy mixing
  aSigL, aSigR pan2 aSig, aLFO1-aLFO2       ; insane panning
  outs aSigL, aSigR             ; stereo output
endin

</CsInstruments>

<CsScore>
;Ins St Dur sinFq lfoFq
  i1 0  2    250    2
  i1 +  2     50   10
  i1 +  2   1500   .5
  i1 +  1     50   10
</CsScore>

</CsoundSynthesizer>
