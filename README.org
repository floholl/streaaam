This is the code repository for /Streaaam/, a *fully automated experimental audio streaming server* developed by students and faculty at the Department of Audio Arts and Acoustics of Columbia College Chicago, using Free/Libre Open Source Software.

- Check out our [[https://streaaam.colum.edu][public demo server]], which we use to showcase sonic art works by students at our department.
- [[doc/setup.org][Instructions]] for how to set up your own /Streaaam/ server on a Raspberry Pi
- [[doc/restart.org][Instructions]] for how to restart your server if it has given up on you
- [[https://streaaam.colum.edu/streaaam.mp4][Video (10')]] explaining how /Streaaam/ works (presentation at 2021 Audio Mostly Conference)
- Accompanying [[https://doi.org/10.1145/3478384.3478426][conference paper]] from 2021 Audio Mostly Conference

#+CAPTION: Streaaam logo by Kevin Block
#+NAME:   fig:streaaam_logo
[[./doc/streaaam_logo.svg]]

#+CAPTION: Streaaam software back end
#+NAME:   fig:streaaam_diagram
[[./doc/streaaam_diagram.svg]]
