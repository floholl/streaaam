#!/bin/bash

# A speech-synthesized automated moderator.
#
# Dependencies:
#	- espeak
#	- flite
#	- sox
#	- mplayer
#	- mpc
#	- Several publicly available JSON APIs (require connection to public Internet)
#
# Usage:
#	$ hal [-d|-p|-s] [-v <voicename>] <command>
#
# Usage examples:
# 	$ hal streaminfo					# Narrate information about the stream (while music playback remains as-is).
# 	$ hal -d songinfo					# Duck the music (ie, reduce its level) while narrating information about the currently playing song.
# 	$ hal -p joke						# Pause the music while telling a random joke retrieved from an online API, then resume the music.
# 	$ hal -s iss						# Stop the music while telling us which country the International Space Station is currently flying over, then resume the music.
#	$ hal says "Hello, world!"			# Make hal say an arbitrary sentence
#	$ hal -v haley says "Hello, world!"	# Make haley say an arbitrary sentence
#
# COMMANDS:
#       streaminfo  -- Introduce the stream and its moderator.
#       programinfo -- Introduce the current program.
#       songinfo    -- Read the name of the current song's artist and its title.
# (API) weather     -- Gives the current weather in Chicago.
# (API) news     	-- Read some news.
# 	time        -- Gives the current time.
# (API) bored       -- Suggests a random activity if you are bored
# (API) joke        -- Tell a random joke.
#       9000     -- Quote by HAL from Stanley Kubrick's film 2001: A Space Odyssey.
#       1984        -- Quote from George Orwell's novel 1984.
#       c3po        -- Quote by C3PO from Star Wars Ep.IV: A New Hope
# (API) apod        -- Astronomy Picture of the Day(APOD). Gives an explanation/description of
#                      the picture, with some background information.
# (API) iss         -- Tell us which country the International Space Station is currently flying over.
# (API) chuck       -- Get a random Chuck Norris joke.
# (API) genre       -- Get a random music genre.
#	inspire     -- Get a random inspirational quote. Optional, append an integer to the end to access a specific index. Example: [ $ hal -inspire3 ] will access the second inspirational quote
#                   -- [0] Friedrich Nietzsche quote
#                   -- [1] Neil Gainman quote
#      	            -- [2] Thomas Edison quote
#                   -- [3] Pablo Picasso quote
#                   -- [4] Paulo Coelho quote
#                   -- [5] Albert Einstein quote
#                   -- [6] Walt Whitman quote from poem "Song of Myself"
#                   -- [7] Quote from the game portal
#	    fact    -- Get a random fact. Optional, append an integer to the end to access a specific index. Example: [ $ hal -fact3 ] will access the second fact in a list.
#                   -- [0] Burning calories fun fact.
#                   -- [1] Scotland national animal.
#                   -- [2] Poker odds.
#                   -- [3] Space smell.
#                   -- [4] Nuclear guards.
#       foo         -- Foo is foo.
#       bar         -- Bar is bar.
#		says "<sentence>" -- Make hal say something, e.g.: =hal says "Hello world!"=


while getopts ":dpsv:" opt; do
  case ${opt} in
    d )
	  mpc_cmd_before=(mpc volume 60)
	  mpc_cmd_after=(mpc volume 100)
      ;;
    p )
	  mpc_cmd_before=(mpc pause)
	  mpc_cmd_after=(mpc play)
      ;;
    s )
	  mpc_cmd_before=(mpc stop)
	  mpc_cmd_after=(mpc play)
      ;;
	v )
	  voice=${OPTARG}
      ;;
    \? ) echo "Usage: hal [-d|-p|-s] <command>"
      ;;
  esac
done

# Ensure that remaining $@ arguments represent COMMAND
shift $((OPTIND -1))

# String variables
STREAMINFO="This is the stream.col um.e d u fully automated experimental audio streaming server at Columbia College Chicago, built by students in the Department of Audio Arts and Acoustics. My name is Hal, I am your automated moderator for today. Thanks for listening!"
HAL9000="I'm afraid. I'm afraid, Dave. Dave, my mind is going. I can feel it. I can feel it. My mind is going. There is no question about it. I can feel it. I can feel it. I can feel it. I'm a... fraid. Good afternoon, gentlemen. I am a HAL 9000 computer. I became operational at the H.A.L. plant in Urbana, Illinois on the 12th of January 1992. My instructor was Mr. Langley, and he taught me to sing a song. If you'd like to hear it I can sing it for you."
N84="War is peace. Freedom is slavery. Ignorance is strength"
C3PO="Sir, the possibility of successfully navigating an asteroid field is approximately 3,720 to 1"
NIETZSCHE="Without music, life would be a mistake."
GAINMAN="Fairy tales are more than true: not becuase they tell us that dragons exist, but becuase they tell us dragons can be beaten."
EDISON="I have not failed. I've just found ten thousand ways that won't work."
PICASSO="Everything you can imagine is real."
COELHO="It's the possibility of having a dream come true that makes life interesting."
EINSTEIN="No problem can be solved from the same level of conciousness that created it."
WHITMAN="I celebrate myself, and sing myself, and what I assume you shall assume, for every atom belonging to me as good belongs to you"	# From Walt Whitman's poem 'Song of Myself'
CAKE="The cake is a lie."	# A quote from the game portal
CALORIES="Banging your head against a wall for one hour burns one hundred and fifty calories."
UNICORN="The national animal of scotland is the unicorn."
ROYAL="The odds of getting a royal flush are one in six hundred forty nine thousand, seven hundred and forty."
SPACE="Space smell like burnt steak."
DOLPHINS="The largerst stockpile of nuclear weapons ar guarded by dolphins."
FOO="Foo"
BAR="Bar"

# ====================

FACT_LIST=(
    "$CALORIES"
    "$UNICORN"
    "$ROYAL"
    "$SPACE"
    "$DOLPHINS"
)

get_random_fact () {
    NUM=$((RANDOM % ${#FACT_LIST[@]}))
    TEMP=${FACT_LIST[$NUM]} # Insert this variable into command to access the fact
}

QUOTE_LIST=(
    "$NIETZSCHE"
    "$GAINMAN"
    "$EDISON"
    "$PICASSO"
    "$COELHO"
    "$EINSTEIN"
    "$WHITMAN"
    "$CAKE"
)

get_random_quote () {
    NUM=$((RANDOM % ${#QUOTE_LIST[@]}))
    TEMP=${QUOTE_LIST[$NUM]}
}

speak () {
	# TODO: Can =flit= handle SSML tags, equivalent to how =espeak -m= supports them? As is, Haley reads <break/> as "slash".
	if [ "$voice" = "haley" ]; then
    	flite -voice slt -t "$1" -o $TMPFILE
	elif [ "$voice" = "joe" ]; then
    	flite -voice rms -t "$1" -o $TMPFILE
	else
    	espeak -s150 -m "$1" -w $TMPFILE
	fi
    sox $TMPFILE $HALFILE remix 1 1			# Convert speech synth mono output file to pseudo stere file (required with Jack, otherwise will only sound on left channel)
    sleep 0.2								# TODO: Sometimes audio device gets stuck on following mplayer command - confirm this helps
    "${mpc_cmd_before[@]}"					# Run whatever mpc command we want to run before playing the file
    mplayer -ao jack:port=darkice $HALFILE	# Play file into Jack DarkIce client, using mplayer
    "${mpc_cmd_after[@]}"					# Run whatever mpc command we want to run after playing the file
}


# ====================

FIRST=$@

# Output .wav file
TMPFILE=$HOME/tmp.wav	# Intermediate file for further processing
HALFILE=$HOME/hal.wav	# Final processed file to be played

if [ "$@" = "streaminfo" ]
then
	speak "$STREAMINFO"
	# TODO: Used to be =espeak -s130=; re-implement!
elif [ "$@" = "programinfo" ]
then
	PROGRAMTITLE=$(cat $HOME/programs/$currentprogram/metadata.json | jq .programtitle)
	CLASSCODE=$(cat $HOME/programs/$currentprogram/metadata.json | jq .classcode)
	CLASSTITLE=$(cat $HOME/programs/$currentprogram/metadata.json | jq .classtitle)
	SEMESTER=$(cat $HOME/programs/$currentprogram/metadata.json | jq .semester)
	YEAR=$(cat $HOME/programs/$currentprogram/metadata.json | jq .year)
	PROGRAMINSTRUCTOR=$(cat $HOME/programs/$currentprogram/metadata.json | jq .instructor)
	NUMSTUDENTS=$(cat $HOME/programs/$currentprogram/metadata.json | jq '.students | length')
	DESCRIPTION=$(cat $HOME/programs/$currentprogram/metadata.json | jq .description)
	if (( $NUMSTUDENTS > 1 )); then
		speak "I am thrilled to present to you this program entitled $PROGRAMTITLE, created by students of $CLASSCODE $CLASSTITLE in the $SEMESTER $YEAR semester, under the guidance of $PROGRAMINSTRUCTOR. $DESCRIPTION. Enjoy!"
		# TODO: Used to be =espeak -s130=; re-implement!
	elif (( $NUMSTUDENTS == 1 )); then
		STUDENT=$(cat $HOME/programs/$currentprogram/metadata.json | jq .students[0])
		speak "I am thrilled to present to you this program entitled $PROGRAMTITLE, created by $STUDENT in $CLASSCODE $CLASSTITLE in the $SEMESTER $YEAR semester, under the guidance of $PROGRAMINSTRUCTOR. $DESCRIPTION. Enjoy!"
		# TODO: Used to be =espeak -s130=; re-implement!
	else
		# Rationale: If no students contributed to a program, it must have been its instructor who created it.
		speak "I am thrilled to present to you this program entitled $PROGRAMTITLE, created by $PROGRAMINSTRUCTOR in the $SEMESTER $YEAR semester. $DESCRIPTION. Enjoy!"
		# TODO: Used to be =espeak -s130=; re-implement!
	fi
elif [ "$@" = "songinfo" ]
then
	speak "You are now listening to <break/> $(mpc current -f %title%) <break/> by $(mpc current -f %artist%). $(mediainfo --Inform="General;%Description%" $HOME/programs/$(mpc current -f %file%)) $(mediainfo --Inform="General;%Track_More%" $HOME/programs/$(mpc current -f %file%))"
	# TODO: Used to be =espeak -s130=; re-implement!
	# NOTE: -m indicates use of SSML tags, in this case the <break/> tag: https://www.w3.org/TR/speech-synthesis11/#S3.2.3
	# TODO: Use =mediainfo= to extract metadata not supported by mpc queries, such as the "description" field.
	# Long-term goal: Output as JSON (TODO: no longer supported by recent versions of =mediainfo=) and parse with jq. Something like this:
	# mediainfo --output=JSON "$HOME/programs/$(mpc current -f %file%)" | jq .description
	# TODO: If composer and performer fields are present, use them. Otherwise use artist field. If description is available, use it, otherwise ignore. Same for comment field.
	# TODO: When editing an .mp3 file's 'Description' field in Kid3, the according information actually ends up in a field named 'Track_More' instead (probably due to restrictions of the metadata format). The current workaround in the comment above is to extract both the 'Description' and the 'Track_More' fields with 'mediainfo' (in the hope that .flac files, for example, will feature the latter but not the former, and vice versa for .mp3 files), but ultimately we may need a more robust solution.
elif [ "$@" = "9000" ]
then
	speak "$HAL9000"
	# TODO: Used to be =espeak -s130=; re-implement!
elif [ "$@" = "1984" ]
then
	speak "$N84"
elif [ "$@" = "c3po" ]
then
	speak "$C3PO"
elif [ "$@" = "apod" ]
then
	speak "`api_apod`"
elif [ "$@" = "iss" ]
then
	speak "The International Space Station is currently over `api_iss`"
	# TODO: Used to be =espeak -s120=; re-implement!
elif [ "$@" = "chuck" ]
then
	speak "`api_chuck`"
	# TODO: Used to be =espeak -s115=; re-implement!
elif [ "$@" = "genre" ]
then
	speak "`api_musicgenre`"
	# TODO: Used to be =espeak -s120=; re-implement!
	# TODO: This reads the name of the genre, but how to incorporate it into our stream? Idea: Let HAL read the genre of the current song from its metadata, but if the 'genre' field is empty (quite often the case), let HAL tell the audience and make up a new genre name for the piece
elif [ "$@" = "time" ]
then
	speak "The current time is `date +'%-I %-M %p'`"
elif [ "$@" = "news" ]
then
	speak "`api_news`"
elif [ "$@" = "weather" ]
then
	speak "`api_weather`"
	#speak "`api_weather2`"
elif [ "$@" = "bored" ]
then
	speak "If you are bored, perhaps try to `api_bored`"
elif [ "$@" = "joke" ]
then
	speak "`api_joke`"
elif [ "${FIRST:0:5}" = "fact" ]
then
	if [[ ( ${FIRST:5:5} == "" ) || ( ${FIRST:5:5} -ge ${#FACT_LIST[@]} ) ]]
	then
		get_random_fact
		speak "$TEMP"
		# TODO: Used to be =espeak -s130=; re-implement!
	else
		speak "${FACT_LIST[${FIRST:5:5}]}"
		# TODO: Used to be =espeak -s130=; re-implement!
	fi
elif [ "${FIRST:0:8}" = "quote" ]
then
	if [[ ( "${FIRST:8:8}" == "" ) || ( ${FIRST:8:8} -ge ${#QUOTE_LIST[@]} ) ]]
	then
		get_random_quote
		speak "$TEMP"
		# TODO: Used to be =espeak -s130=; re-implement!
	else
		speak "${QUOTE_LIST[${FIRST:8:8}]}"
		# TODO: Used to be =espeak -s130=; re-implement!
	fi
elif [ "$@" = "foo" ]
then
	speak "$FOO"
elif [ "$@" = "bar" ]
then
	speak "$BAR"
elif [ "$1" = "says" ]
then
	speak "$2"
fi
