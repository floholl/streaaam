#!/bin/bash


###### DO NOT EDIT THIS FIRST SECTION! #######

# NOTE: Do /not/ put $PIDFILE into /tmp (which is regularly emptied)
# Source: https://bencane.com/2015/09/22/preventing-duplicate-cron-job-executions/
PIDFILE=$HOME/cronjob.pid

# If a PID file exists, terminate the process that created it (i.e., the previous cron job)
if [ -f $PIDFILE ]
then
  pkill -F $PIDFILE
fi

# Create a new PID file (for the next cron job to terminate this one) and check it worked
echo $$ > $PIDFILE  # Write current process ID to $PIDFILE
if [ $? -ne 0 ]     # If last command ($?) did not successfully exit with 0...
then
  echo "Could not create PID file"
  exit 1            # ... terminate this script with an error.
fi

# If a Pd job is running, terminate it
PDPID=$HOME/pd.pid
if [ -f $PDPID ]
then
  pkill -F $PDPID
fi

# If a SuperCollider server job is running, terminate it
SCPID=$HOME/sc.pid
if [ -f $SCPID ]
then
  pkill -F $SCPID
fi

# If a ChucK server job is running, terminate it
CHUCKPID=$HOME/chuck.pid
if [ -f $CHUCKPID ]
then
  pkill -F $CHUCKPID
fi


###### Edit the following section according to your needs ######

export currentprogram=$1

# Clear current playlist
mpc clear

# List /all/ tracks in the MPD library and add them to a looong playlist
mpc ls | mpc add

# Make sure playlist repeats again from beginning when exhausted
mpc repeat on

# Randomize playback
mpc random on

# Turn single option off
mpc single off

# Turn consume option off
mpc consume off

# Start playback, then wait a bit
mpc play
sleep 10

# Read info on stream, then wait a bit
hal -d streaminfo
sleep 3

# Read info on current program, then wait a bit
hal -d programinfo
sleep 6

# For first song, we need to read song info outside of while loop below in order to catch its event
hal -d songinfo

# For as long as this script is running...
while true
do
  mpc idle player	# ... every time an MPD 'player' event is detected (e.g., start of new song)
  sleep 8
  hal -d songinfo	# ... read info on current song.
done

exit 0
