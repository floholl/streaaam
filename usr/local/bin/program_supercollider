#!/bin/bash

# Load a randomized sequence of SuperCollider patches.
#
# Usage:
#       $ program_sc <programname>
#
# Dependencies:
#       - /usr/local/bin/sssc
#       - xvfb-run from xvfb package
#       - sclang


###### DO NOT EDIT THIS FIRST SECTION! #######

# NOTE: Do /not/ put $PIDFILE into /tmp (which is regularly emptied)
# Source: https://bencane.com/2015/09/22/preventing-duplicate-cron-job-executions/
PIDFILE=$HOME/cronjob.pid

# If a PID file exists, terminate the process that created it (i.e., the previous cron job)
if [ -f $PIDFILE ]
then
  pkill -F $PIDFILE
fi

# Create a new PID file (for the next cron job to terminate this one) and check it worked
echo $$ > $PIDFILE  # Write current process ID to $PIDFILE
if [ $? -ne 0 ]     # If last command ($?) did not successfully exit with 0...
then
  echo "Could not create PID file"
  exit 1            # ... terminate this script with an error.
fi

# If a Pd job is running, terminate it
PDPID=$HOME/pd.pid
if [ -f $PDPID ]
then
  pkill -F $PDPID
fi

# If a SuperCollider server job is running, terminate it
SCPID=$HOME/sc.pid
if [ -f $SCPID ]
then
  pkill -F $SCPID
fi

# If a ChucK server job is running, terminate it
CHUCKPID=$HOME/chuck.pid
if [ -f $CHUCKPID ]
then
  pkill -F $CHUCKPID
fi


###### Edit the following section according to your needs ######

# Stop the music
mpc stop

# Start SuperCollider server before hal annnouncement, so it has some time to establish itself
server_supercollider

# Read info on stream, then wait a bit
hal streaminfo

# Read info on current program
export currentprogram=$1
hal programinfo

# For as long as this script is running...
while true
do


  # Pick a random patch from the playlist ('shuf' idea first implemented by PT in fall 2020 as =rndfile= script)
  patch=$(shuf -n 1 $HOME/programs/$currentprogram/playlist)
  # TODO: Randomization should be optional. Aim to re-implement ALL of mpc's repeat|random|single|consume features.
  # TODO: Check whether previous patch has been re-picked, and if so, pick another one.

  # TODO Extract TITLE:, ARTIST:, and DESCRIPTION: comments from current patch
  #title=$(grep TITLE: $HOME/programs/$currentprogram/$patch | sed 's/^.*TITLE:\(.*\);/\1/'  | sed -e 's/^[ \t]*//')
  #artist=$(grep ARTIST: $HOME/programs/$currentprogram/$patch | sed 's/^.*ARTIST:\(.*\);/\1/'  | sed -e 's/^[ \t]*//')
  #description=$(grep DESCRIPTION: $HOME/programs/$currentprogram/$description | sed 's/^.*DESCRIPTION:\(.*\);/\1/'  | sed -e 's/^[ \t]*//')

 # hal says "You will now be listening to <break/> $patch."
  #hal says "You will now be listening to <break/> $title <break/> by $artist. $description"
  # TODO: How to make announcement when track is already playing AND turn the volume down, analogous to -d?
  # TODO: Merge with 'hal songinfo'?

  # Open a patch and let it play for some time
  #timeout 60 xvfb-run --auto-servernum sclang programs/$currentprogram/$patch  # TODO: Make 60s time interval per patch an argument for the current script!
  #timeout 60 xvfb-run --auto-servernum sclang run_patch.scd  # TODO: Make 60s time interval per patch an argument for the current script!
  # TODO: Timeout not working as expected when run from cron job within while loop. The loop itself is working (hal makes another announcement), but neither is the old patch stopped nor the new one loaded). Note that it works when reproducing a sequence of two of the above commands own on command line (also with timeout, and with xvfb-run (although without while loop) - neither seems to be the issue here.
  #sleep 60  # TODO: Make 60s time interval per patch an argument for the current script!
  xvfb-run --auto-servernum sclang run_patch.scd	# TODO: This works on streaaam.colum.edu, but fails on RPi, where sclang kills the running scsynth and starts a new one that will then of curse not connect to DarkIce. I am thinking this is due to different SuperCollider builds on Ubuntu vs. RPi OS. Note I /have to/ start scsynth independently in order to then cleanly stop it when the next program starts.

done

exit 0
