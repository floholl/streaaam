#!/bin/bash

# News program with Chicago weather report, featuring Hal, Haley, and Joe.
#
# Usage:
#	$ program_news
#
# Dependencies:
#	- /usr/local/bin/hal
#	- /usr/local/bin/server_pd


###### DO NOT EDIT THIS FIRST SECTION! #######

# NOTE: Do /not/ put $PIDFILE into /tmp (which is regularly emptied)
# Source: https://bencane.com/2015/09/22/preventing-duplicate-cron-job-executions/
PIDFILE=$HOME/cronjob.pid

# If a PID file exists, terminate the process that created it (i.e., the previous cron job)
if [ -f $PIDFILE ]
then
  pkill -F $PIDFILE
fi

# Create a new PID file (for the next cron job to terminate this one) and check it worked
echo $$ > $PIDFILE  # Write current process ID to $PIDFILE
if [ $? -ne 0 ]     # If last command ($?) did not successfully exit with 0...
then
  echo "Could not create PID file"
  exit 1            # ... terminate this script with an error.
fi

# If a Pd job is running, terminate it
PDPID=$HOME/pd.pid
if [ -f $PDPID ]
then
  pkill -F $PDPID
fi

# If a SuperCollider server job is running, terminate it
SCPID=$HOME/sc.pid
if [ -f $SCPID ]
then
  pkill -F $SCPID
fi

# If a ChucK server job is running, terminate it
CHUCKPID=$HOME/chuck.pid
if [ -f $CHUCKPID ]
then
  pkill -F $CHUCKPID
fi


###### Edit the following section according to your needs ######

# Stop the music
mpc stop

# Start Pd server
# HACK! Move to program directory first, or else abstractions used in greenwich.pd will not load!
#       Works also with cron, as confirmed by https://unix.stackexchange.com/a/98546
cd programs/news && server_pd &

# Save the PID $!) of the last child process (server_pd) to a variable
server_pd_pid=$!

# Open Pd patch and let it play until it quits Pd itself
# NOTE: sleep 2 seems to indeed be required between starting server_pd and loading a patch, as confirmed by a 2021-12-03 test.
#       Surprisingly, it does /not/ seem to help to put the sleep 2 command directly into server_pd itself (I tried this).
sleep 2 && pdmsgr open greenwich.pd

# Wait for patch itself to quit pd, which will in turn terminate server_pd
wait $server_pd_pid

# Read info on stream
hal streaminfo

# Read info on current program
hal says "And now the news, brought to you by my colleague Haley."

# Announce the current time
hal -v haley says "Thanks, Hal."
hal -v haley time

# Read the current news
hal -v haley news

# Current weather in Chicago
hal -v haley says "And now my colleague Joe will tell us about the current weather in Chicago. Joe?"
hal -v joe says "Thanks, Haley."
hal -v joe weather

exit 0
